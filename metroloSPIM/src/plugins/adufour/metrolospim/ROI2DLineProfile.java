package plugins.adufour.metrolospim;

import icy.image.IcyBufferedImage;
import icy.plugin.abstract_.Plugin;
import icy.type.collection.array.Array1DUtil;

import java.awt.geom.Point2D;

public class ROI2DLineProfile extends Plugin
{
    public static double[][] getSegmentProfile(Point2D p1, Point2D p2, IcyBufferedImage image)
    {
        int distance = (int) p1.distance(p2);
        
        double vx = (p2.getX() - p1.getX()) / distance;
        double vy = (p2.getY() - p1.getY()) / distance;
        
        int nbComponent = image.getSizeC();
        double[][] data = new double[nbComponent][distance];
        
        double x = p1.getX();
        double y = p1.getY();
        
        for (int i = 0; i < distance; i++)
        {
            if (image.isInside((int) x, (int) y))
            {
                for (int component = 0; component < nbComponent; component++)
                {
                    data[component][i] = Array1DUtil.getValue(image.getDataXY(component), image.getOffset((int) x, (int) y), image.isSignedDataType());
                }
            }
            else
            {
                for (int component = 0; component < nbComponent; component++)
                {
                    data[component][i] = 0.0D;
                }
            }
            
            x += vx;
            y += vy;
        }
        
        return data;
    }
}
