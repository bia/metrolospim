package plugins.adufour.metrolospim;

import icy.canvas.IcyCanvas;
import icy.painter.Overlay;
import icy.plugin.abstract_.Plugin;
import icy.roi.ROI;
import icy.roi.ROI2D;
import icy.roi.ROIUtil;
import icy.sequence.Sequence;
import icy.util.GraphicsUtil;
import icy.util.ShapeUtil.BooleanOperator;
import icy.util.StringUtil;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Arrays;

import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarBoolean;
import plugins.adufour.vars.lang.VarDouble;
import plugins.adufour.vars.lang.VarSequence;
import plugins.kernel.roi.roi2d.ROI2DArea;
import plugins.kernel.roi.roi2d.ROI2DPolygon;

public class ComputeQuadrants extends Plugin implements Block
{
    VarSequence input            = new VarSequence("Sequence", null);
    
    VarBoolean  splitExistingROI = new VarBoolean("Split ROI", true);
    
    Overlay     overlay;
    
    VarDouble   north            = new VarDouble("North (%)", 0.0);
    VarDouble   east             = new VarDouble("East (%)", 0.0);
    VarDouble   west             = new VarDouble("West (%)", 0.0);
    VarDouble   south            = new VarDouble("South (%)", 0.0);
    
    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("sequence", input);
        inputMap.add("split", splitExistingROI);
    }
    
    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("north", north);
        outputMap.add("east", east);
        outputMap.add("west", west);
        outputMap.add("south", south);
    }
    
    @Override
    public void run()
    {
        Sequence sequence = input.getValue();
        
        // create each quadrant as a polygon
        
        int w = sequence.getWidth();
        int h = sequence.getHeight();
        
        final ROI2DPolygon[] quadrants = new ROI2DPolygon[4];
        
        // north
        quadrants[0] = new ROI2DPolygon(new Point2D.Double(0, 0));
        quadrants[0].addPoint(new Point2D.Double(w / 2.0, -0.5 + h / 2.0), true);
        quadrants[0].addPoint(new Point2D.Double(w, 0), true);
        
        // west
        quadrants[1] = new ROI2DPolygon(new Point2D.Double(0, h - 0.5));
        quadrants[1].addPoint(new Point2D.Double(-0.5 + w / 2.0, h / 2.0), true);
        quadrants[1].addPoint(new Point2D.Double(0, 0), true);
        
        // east
        quadrants[2] = new ROI2DPolygon(new Point2D.Double(w + 0.5, 0));
        quadrants[2].addPoint(new Point2D.Double(0.5 + w / 2.0, h / 2.0), true);
        quadrants[2].addPoint(new Point2D.Double(w + 0.5, h + 0.5), true);
        
        // south
        quadrants[3] = new ROI2DPolygon(new Point2D.Double(w + 0.5, h + 0.5));
        quadrants[3].addPoint(new Point2D.Double(w / 2.0, 0.5 + h / 2.0), true);
        quadrants[3].addPoint(new Point2D.Double(0, h + 0.5), true);
        
        for (ROI2DPolygon quadrant : quadrants)
            quadrant.setColor(Color.blue);
        
        final double[] areas = new double[4];
        
        // merge all existing sequence ROI into a single area
        
        ArrayList<ROI> rois = sequence.getROIs();
        if (rois.size() > 0)
        {
            ROI2D merge = (ROI2D) ROIUtil.merge(rois, BooleanOperator.OR);
            ROI2DArea mergeArea = new ROI2DArea(merge.getBooleanMask(true));
            
            double totalArea = mergeArea.getNumberOfPoints();
            
            if (totalArea > 0)
            {
                // compute the surface ratio inside each quadrant
                
                for (int i = 0; i < 4; i++)
                {
                    // intersect the quadrant with the total surface
                    areas[i] = ROIUtil.merge(Arrays.asList(quadrants[i], (ROI) mergeArea), BooleanOperator.AND).getNumberOfPoints();
                    
                    // make it a ratio
                    areas[i] /= totalArea;
                }
                
                for (int i = 0; i < 4; i++)
                {
                    // set a gradual color in "restricted" HSV space
                    // => from blue (240', i.e. 2f/3f) to red (0', or 0f)
                    
                    // scale the areas to get a [2/3,0] interval
                    double hsv = (2.0 / 3.0) - (areas[i] * 2.0 / 3.0);
                    
                    quadrants[i].setColor(Color.getHSBColor((float) hsv, 1f, 1f));
                    
                    // make it a percentage for display purposes
                    areas[i] *= 100;
                }
            }
        }
        
        north.setValue(areas[0]);
        west.setValue(areas[1]);
        east.setValue(areas[2]);
        south.setValue(areas[3]);
        
        // remove the overlay if it existed
        if (overlay != null) sequence.removeOverlay(overlay);
        
        sequence.addOverlay(overlay = new Overlay("Quadrants")
        {
            @Override
            public void paint(Graphics2D g, Sequence sequence, IcyCanvas canvas)
            {
                g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                
                float fontSize = (float) canvas.canvasToImageLogDeltaX(30);
                g.setFont(new Font("Trebuchet MS", Font.BOLD, 10).deriveFont(fontSize));
                
                double stroke = Math.max(canvas.canvasToImageLogDeltaX(2), canvas.canvasToImageLogDeltaY(2));
                
                g.setStroke(new BasicStroke((float) stroke, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
                
                for (int i = 0; i < 4; i++)
                {
                    ROI2DPolygon quadrant = quadrants[i];
                    
                    Rectangle bounds = quadrant.getBounds();
                    String text = StringUtil.toString(areas[i], 2) + "%";
                    Dimension size = GraphicsUtil.getHintSize(g, text);
                    
                    int x = (int) bounds.getCenterX() - size.width / 2;
                    int y = (int) bounds.getCenterY() - size.height / 2;
                    
                    g.setColor(quadrant.getColor());
                    
                    g.draw(quadrant);
                    
                    GraphicsUtil.drawHint(g, text, x, y, quadrant.getColor(), quadrant.getColor());
                }
            }
        });
    }
}
