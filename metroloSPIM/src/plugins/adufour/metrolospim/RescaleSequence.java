package plugins.adufour.metrolospim;

import icy.image.IcyBufferedImage;
import icy.image.IcyBufferedImageUtil;
import icy.image.IcyBufferedImageUtil.FilterType;
import icy.plugin.abstract_.Plugin;
import icy.sequence.Sequence;
import icy.system.SystemUtil;
import icy.type.DataType;
import icy.type.collection.array.Array1DUtil;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.gui.model.DoubleRangeModel;
import plugins.adufour.vars.lang.VarDouble;
import plugins.adufour.vars.lang.VarSequence;

public class RescaleSequence extends Plugin implements SequenceBlock
{
    VarSequence varSeq         = new VarSequence("Sequence", null);
    VarDouble   varScale       = new VarDouble("Scale factor", 0.5);
    
    VarSequence varSeqRescaled = new VarSequence("Rescaled sequence", null);
    
    @Override
    public void run()
    {
        Sequence in = varSeq.getValue(true);
        final Sequence out = new Sequence("Rescaled " + in.getName());
        
        final int newWidth = (int) (in.getSizeX() * varScale.getValue());
        final int newHeight = (int) (in.getSizeY() * varScale.getValue());
        final int newDepth = (int) (in.getSizeZ() * varScale.getValue());
        
        final double scale = varScale.getValue();
        
        final boolean downScale = (scale < 1.0);
        
        final double gridStep = 1 / scale;
        
        final int sizeT = in.getSizeT();
        final int sizeZ = in.getSizeZ();
        final int sizeC = in.getSizeC();
        final int sizeX = in.getSizeX();
        final DataType dataType = in.getDataType_();
        
        if (downScale)
        {
            for (int t = 0; t < sizeT; t++)
                for (int z = 0; z < newDepth; z++)
                {
                    out.setImage(t, z, new IcyBufferedImage(newWidth, newHeight, sizeC, dataType));
                }
        }
        
        ExecutorService service = Executors.newFixedThreadPool(SystemUtil.getAvailableProcessors());
        
        ArrayList<Callable<Object>> tasks = new ArrayList<Callable<Object>>(sizeZ * sizeT);
        
        for (int t = 0; t < sizeT; t++)
        {
            for (int k = 0; k < newDepth; k++)
            {
                int slice = (int) (k * gridStep);
                
                if (downScale)
                {
                    // optimize the process manually
                    
                    final Object _in = in.getDataXYC(t, slice);
                    final Object _out = out.getDataXYC(t, k);
                    
                    tasks.add(new Callable<Object>()
                    {
                        @Override
                        public Object call()
                        {
                            for (int c = 0; c < sizeC; c++)
                            {
                                Object _inXY = Array.get(_in, c);
                                Object _outXY = Array.get(_out, c);
                                double y = gridStep * 0.5;
                                
                                int off = 0;
                                for (int j = 0; j < newHeight; j++)
                                {
                                    double x = gridStep * 0.5;
                                    
                                    for (int i = 0; i < newWidth; i++, off++)
                                    {
                                        double value = Array1DUtil.getValue(_inXY, (int) (x + y * sizeX), dataType);
                                        Array1DUtil.setValue(_outXY, off, dataType, value);
                                        x += gridStep;
                                        
                                    }
                                    
                                    y += gridStep;
                                }
                            }
                            return null;
                        }
                    });
                }
                else
                {
                    final IcyBufferedImage inSlice = in.getImage(t, slice);
                    final int outT = t;
                    final int outZ = k;
                    
                    tasks.add(new Callable<Object>()
                    {
                        @Override
                        public Object call()
                        {
                            out.setImage(outT, outZ, IcyBufferedImageUtil.scale(inSlice, newWidth, newHeight, FilterType.NEAREST));
                            return null;
                        }
                    });
                }
            }
        }
        
        try
        {
            service.invokeAll(tasks);
        }
        catch (InterruptedException e)
        {
        }
        finally
        {
            service.shutdownNow();
        }
        
        varSeqRescaled.setValue(out);
    }
    
    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("input sequence", varSeq);
        varScale.setDefaultEditorModel(new DoubleRangeModel(0.5, 0.001, 1000.0, 0.001));
        inputMap.add("scale factor", varScale);
    }
    
    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("output sequence", varSeqRescaled);
    }
    
}
