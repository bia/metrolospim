package plugins.adufour.metrolospim;

import icy.plugin.abstract_.Plugin;
import icy.roi.ROI;
import icy.roi.ROI2D;
import icy.type.rectangle.Rectangle5D;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarDoubleArrayNative;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.kernel.roi.roi3d.ROI3DStack;

public class GetROIMassCenter extends Plugin implements ROIBlock
{
    VarROIArray          roiArray = new VarROIArray("List of ROI");
    
    VarDoubleArrayNative xCenter  = new VarDoubleArrayNative("Centers (X)", null);
    
    VarDoubleArrayNative yCenter  = new VarDoubleArrayNative("Centers (Y)", null);
    
    VarDoubleArrayNative zCenter  = new VarDoubleArrayNative("Centers (Z)", null);
    
    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("inputROI", roiArray);
    }
    
    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("center (X)", xCenter);
        outputMap.add("center (Y)", yCenter);
        outputMap.add("center (Z)", zCenter);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public void run()
    {
        int length = roiArray.size();
        
        xCenter.setValue(new double[length]);
        yCenter.setValue(new double[length]);
        zCenter.setValue(new double[length]);
        
        int cpt = -1;
        for (ROI roi : roiArray.getValue())
        {
            cpt++;
            
            Rectangle5D r5 = roi.computeBounds5D();
            
            if (roi instanceof ROI3DStack)
            {
                boolean firstSlice = true;
                
                for (ROI2D slice : (ROI3DStack<ROI2D>) roi)
                {
                    if (firstSlice)
                    {
                        r5 = slice.computeBounds5D();
                        firstSlice = false;
                    }
                    else r5.add(slice.getBounds5D());
                }
            }
            
            xCenter.getValue()[cpt] = r5.getCenterX();
            yCenter.getValue()[cpt] = r5.getCenterY();
            zCenter.getValue()[cpt] = r5.getCenterZ();
        }
    }
}
