package plugins.adufour.metrolospim;

import icy.plugin.abstract_.Plugin;
import icy.roi.ROI;
import icy.roi.ROI2D;

import java.awt.Point;
import java.util.Arrays;

import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.kernel.roi.roi2d.ROI2DArea;
import plugins.kernel.roi.roi2d.ROI2DEllipse;
import plugins.kernel.roi.roi2d.ROI2DShape;

/**
 * @author Alexandre Dufour
 */
public class FitCircle extends Plugin implements Block
{
	VarROIArray roiArray = new VarROIArray("List of ROI");
	VarROIArray roiOUT = new VarROIArray("List of circles");

	/**
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run()
	{
		for (ROI roi : roiArray.getValue())
		{
			if (roi instanceof ROI2D)
			{
				ROI2DEllipse circle = new ROI2DEllipse();

				if (roi instanceof ROI2DArea)
				{
					ROI2DArea area = (ROI2DArea) roi;
					Point[] points = area.getBooleanMask(true).getContourPoints();
					circle.setToFitCircle(Arrays.asList(points));
				}
				else if (roi instanceof ROI2DShape)
				{
					ROI2DShape shape = (ROI2DShape) roi;
					circle.setToFitCircle(shape.getPoints());
				}

				roiOUT.add(circle);
			}
		}
	}

	/**
	 * @see plugins.adufour.blocks.lang.Block#declareInput(plugins.adufour.blocks.util.VarList)
	 */
	@Override
	public void declareInput(VarList inputMap)
	{
		inputMap.add("input ROI array", roiArray);
	}

	/**
	 * @see plugins.adufour.blocks.lang.Block#declareOutput(plugins.adufour.blocks.util.VarList)
	 */
	@Override
	public void declareOutput(VarList outputMap)
	{
		outputMap.add("output ROI array", roiOUT);
	}
}
