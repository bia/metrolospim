package plugins.adufour.metrolospim;

import icy.plugin.abstract_.Plugin;
import icy.sequence.Sequence;
import icy.sequence.SequenceUtil;

import java.awt.Rectangle;

import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.gui.model.IntegerRangeModel;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.lang.VarSequence;
import plugins.kernel.roi.roi2d.ROI2DRectangle;

public class ExtractEquator extends Plugin implements Block
{
    VarSequence seq       = new VarSequence("input", null);
    VarInteger  thickness = new VarInteger("thickness (% of total height)", 20);
    VarSequence equator   = new VarSequence("equator", null);
    
    @Override
    public void run()
    {
        Sequence in = seq.getValue(true);
        
        int newHeight = (int) (in.getHeight() / 100.0 * thickness.getValue());
        
        int centerY = in.getSizeY() / 2;
        int newY = centerY - newHeight / 2;
        
        ROI2DRectangle rectangle = new ROI2DRectangle(new Rectangle(0, newY, in.getSizeX(), newHeight));
        
        equator.setValue(SequenceUtil.getSubSequence(in, rectangle));
    }
    
    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("input sequence", seq);
        thickness.setDefaultEditorModel(new IntegerRangeModel(20, 1, 100, 1));
        inputMap.add("thickness (% of height)", thickness);
    }
    
    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("output equator", equator);
    }
    
}
