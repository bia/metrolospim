package plugins.adufour.metrolospim;

import icy.plugin.abstract_.Plugin;
import icy.sequence.SequenceDataIterator;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarDouble;
import plugins.adufour.vars.lang.VarSequence;

public class Sum extends Plugin implements Block
{
    VarSequence seq = new VarSequence("sequence", null);
    VarDouble   sum = new VarDouble("sum", 0);
    
    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add(seq);
    }
    
    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add(sum);
    }
    
    @Override
    public void run()
    {
        double d = 0;
        
        SequenceDataIterator it = new SequenceDataIterator(seq.getValue(true));
        while (!it.done())
        {
            d += it.get();
            it.next();
        }
        
        sum.setValue(d);
    }
    
}
