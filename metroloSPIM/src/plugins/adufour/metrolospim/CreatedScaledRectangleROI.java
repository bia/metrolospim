package plugins.adufour.metrolospim;

import icy.plugin.abstract_.Plugin;
import icy.roi.ROI;
import icy.roi.ROI2D;
import icy.roi.ROI3D;
import icy.type.point.Point3D;
import icy.type.rectangle.Rectangle5D;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.roi.ROI3DRectangle;
import plugins.adufour.vars.lang.VarDouble;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.kernel.roi.roi2d.ROI2DRectangle;
import plugins.kernel.roi.roi3d.ROI3DStack;

public class CreatedScaledRectangleROI extends Plugin implements ROIBlock
{
    VarROIArray inROI       = new VarROIArray("list of ROI");
    VarDouble   scaleFactor = new VarDouble("scale", 2);
    VarROIArray rescaledROI = new VarROIArray("list of rescaled ROI");
    
    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("input ROI", inROI);
        inputMap.add("scale factor", scaleFactor);
    }
    
    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("output ROI", rescaledROI);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public void run()
    {
        double scale = scaleFactor.getValue();
        
        for (ROI roi : inROI.getValue(true))
        {
            if (roi instanceof ROI2D)
            {
                Rectangle2D r = ((ROI2D) roi).getBounds2D();
                Point2D.Double topLeft = new Point2D.Double(r.getMinX() * scale, r.getMinY() * scale);
                Point2D.Double bottomRight = new Point2D.Double(r.getMaxX() * scale, r.getMaxY() * scale);
                ROI2DRectangle rect = new ROI2DRectangle(topLeft, bottomRight);
                rescaledROI.add(rect);
            }
            if (roi instanceof ROI3D)
            {
                Rectangle5D r5 = roi.getBounds5D();
                
                if (roi instanceof ROI3DStack)
                {
                    // recompute bounds (and headshoot Stephane)
                    boolean firstSlice = true;
                    for (ROI2D slice : (ROI3DStack<ROI2D>) roi)
                    {
                        if (firstSlice)
                        {
                            r5 = slice.computeBounds5D();
                            firstSlice = false;
                        }
                        else r5.add(slice.getBounds5D());
                    }
                }
                
                Point3D.Double topLeftUp = new Point3D.Double(r5.getX() * scale, r5.getY() * scale, r5.getZ() * scale);
                Point3D.Double bottomRightDown = new Point3D.Double(r5.getSizeX() * scale, r5.getSizeY() * scale, r5.getSizeZ() * scale);
                ROI3DRectangle rect = new ROI3DRectangle(topLeftUp, bottomRightDown);
                rescaledROI.add(rect);
            }
            else throw new UnsupportedOperationException("Warning: ROI of type " + roi.getClassName() + " are not currently supported");
        }
    }
    
}
