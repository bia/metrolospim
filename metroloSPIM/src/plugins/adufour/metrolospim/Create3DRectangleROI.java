package plugins.adufour.metrolospim;

import icy.plugin.abstract_.Plugin;
import icy.type.point.Point3D;
import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.roi.ROI3DRectangle;
import plugins.adufour.vars.lang.VarDoubleArrayNative;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.adufour.vars.util.VarException;

public class Create3DRectangleROI extends Plugin implements ROIBlock
{
    VarDoubleArrayNative xCenter = new VarDoubleArrayNative("Centers (X)", null);
    
    VarDoubleArrayNative yCenter = new VarDoubleArrayNative("Centers (Y)", null);
    
    VarDoubleArrayNative zCenter = new VarDoubleArrayNative("Centers (Z)", null);
    
    VarDoubleArrayNative xSize   = new VarDoubleArrayNative("Width(s)", null);
    
    VarDoubleArrayNative ySize   = new VarDoubleArrayNative("Height(s)", null);
    
    VarDoubleArrayNative zSize   = new VarDoubleArrayNative("Depth(s)", null);
    
    VarROIArray          roi     = new VarROIArray("List of ROI");
    
    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("center (X)", xCenter);
        inputMap.add("center (Y)", yCenter);
        inputMap.add("center (Z)", zCenter);
        inputMap.add("sizes (X)", xSize);
        inputMap.add("sizes (Y)", ySize);
        inputMap.add("sizes (Z)", zSize);
    }
    
    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add(roi);
    }
    
    @Override
    public void run()
    {
        int n = xCenter.size();
        if (n != yCenter.size() || n != zCenter.size()) throw new VarException("[Create 3D Rectangle ROI] The lists of centers don't have the same number of elements");
        
        int nSize = xSize.size();
        if (nSize != ySize.size() || nSize != zSize.size()) throw new VarException("[Create 3D Rectangle ROI] The lists of sizes don't have the same number of elements");
        
        if (nSize != 1 && nSize != n) throw new VarException("[Create 3D Rectangle ROI] The number of sizes must be equal to 1 or the number of centers");
        
        for (int i = 0; i < n; i++)
        {
            // get the dimensions
            double sizeX, sizeY, sizeZ;
            
            if (nSize == 1)
            {
                sizeX = xSize.getValue()[0];
                sizeY = ySize.getValue()[0];
                sizeZ = zSize.getValue()[0];
            }
            else
            {
                sizeX = xSize.getValue()[i];
                sizeY = ySize.getValue()[i];
                sizeZ = zSize.getValue()[i];
            }
            
            // create the ROI
            
            double minX = xCenter.getValue()[i] - sizeX / 2;
            double minY = yCenter.getValue()[i] - sizeY / 2;
            double minZ = zCenter.getValue()[i] - sizeZ / 2;
            
            roi.add(new ROI3DRectangle(new Point3D.Double(minX, minY, minZ), new Point3D.Double(sizeX, sizeY, sizeZ)));
        }
    }
}
