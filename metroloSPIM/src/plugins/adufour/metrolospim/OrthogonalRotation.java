package plugins.adufour.metrolospim;

import icy.image.IcyBufferedImage;
import icy.plugin.abstract_.Plugin;
import icy.sequence.Sequence;
import icy.type.DataType;

import java.lang.reflect.Array;

import plugins.adufour.blocks.tools.sequence.SequenceBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarEnum;
import plugins.adufour.vars.lang.VarSequence;

public class OrthogonalRotation extends Plugin implements SequenceBlock
{
    public enum Orientation
    {
        X_becomes_Y, Y_becomes_X, X_becomes_Z, Y_becomes_Z, Z_becomes_X, Z_becomes_Y
    }
    
    VarSequence          input       = new VarSequence("Sequence", null);
    
    VarEnum<Orientation> orientation = new VarEnum<OrthogonalRotation.Orientation>("Orienation", Orientation.Z_becomes_Y);
    
    VarSequence          output      = new VarSequence("Rotated sequence", null);
    
    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("input sequence", input);
        inputMap.add("orientation", orientation);
    }
    
    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("output sequence", output);
    }
    
    @Override
    public void run()
    {
        Sequence in = input.getValue(true);
        Orientation or = orientation.getValue(true);
        Sequence out = null;
        
        int sizeX = in.getSizeX();
        int sizeY = in.getSizeY();
        int sizeXY = sizeX * sizeY;
        int sizeZ = in.getSizeZ();
        int sizeC = in.getSizeC();
        int sizeT = in.getSizeT();
        DataType dataType = in.getDataType_();
        
        int startOffset = 0;
        int sign = 1;
        
        switch (or)
        {
            case X_becomes_Y:
            case Y_becomes_X:
                
                startOffset = (or == Orientation.X_becomes_Y ? sizeXY - sizeY : sizeY - 1);
                sign = (or == Orientation.X_becomes_Y ? 1 : -1);
                
                // out has inverted XY dimensions
                out = new Sequence();
                for (int t = 0; t < sizeT; t++)
                    for (int z = 0; z < sizeZ; z++)
                        out.setImage(t, z, new IcyBufferedImage(sizeY, sizeX, sizeC, dataType));
                
                for (int t = 0; t < sizeT; t++)
                    for (int c = 0; c < sizeC; c++)
                        for (int z = 0; z < sizeZ; z++)
                        {
                            final Object in_XY = in.getDataXY(t, z, c);
                            final Object out_XY = out.getDataXY(t, z, c);
                            
                            // browse each line of the input and store in the output accordingly
                            
                            for (int j = 0; j < sizeY; j++)
                            {
                                int inOffset = j * sizeX;
                                int outOffset = startOffset + j * sign;
                                
                                for (int i = 0; i < sizeX; i++, inOffset++, outOffset -= sizeY * sign)
                                {
                                    Array.set(out_XY, outOffset, Array.get(in_XY, inOffset));
                                }
                            }
                        }
            break;
            
            case X_becomes_Z:
            case Z_becomes_X:
                
                startOffset = (or == Orientation.X_becomes_Z ? sizeZ - 1 : 0);
                sign = (or == Orientation.X_becomes_Z ? -1 : 1);
                
                // out has inverted XZ dimensions
                out = new Sequence();
                for (int t = 0; t < sizeT; t++)
                    for (int x = 0; x < sizeX; x++)
                        out.setImage(t, x, new IcyBufferedImage(sizeZ, sizeY, sizeC, dataType));
                
                for (int t = 0; t < sizeT; t++)
                    for (int c = 0; c < sizeC; c++)
                    {
                        final Object out_Z_XY = out.getDataXYZ(t, c);
                        
                        for (int z = 0; z < sizeZ; z++)
                        {
                            final Object in_XY = in.getDataXY(t, z, c);
                            
                            // browse each line of the input and store in the output accordingly
                            
                            for (int j = 0; j < sizeY; j++)
                            {
                                int inOffset = j * sizeX;
                                int outOffset = startOffset + j * sizeZ + z * sign;
                                
                                for (int i = 0; i < sizeX; i++, inOffset++)
                                {
                                    Object outSlice = Array.get(out_Z_XY, i);
                                    Array.set(outSlice, outOffset, Array.get(in_XY, inOffset));
                                }
                            }
                        }
                    }
            break;
            
            case Y_becomes_Z:
            case Z_becomes_Y:
                
                startOffset = (or == Orientation.Z_becomes_Y ? 0 : sizeY - 1);
                sign = (or == Orientation.Z_becomes_Y ? 1 : -1);
                
                // out has inverted YZ dimensions
                out = new Sequence();
                for (int t = 0; t < sizeT; t++)
                    for (int y = 0; y < sizeY; y++)
                        out.setImage(t, y, new IcyBufferedImage(sizeX, sizeZ, sizeC, dataType));
                
                for (int t = 0; t < sizeT; t++)
                    for (int c = 0; c < sizeC; c++)
                    {
                        final Object out_Z_XY = out.getDataXYZ(t, c);
                        
                        for (int z = 0; z < sizeZ; z++)
                        {
                            final Object in_XY = in.getDataXY(t, z, c);
                            
                            int outOffset = (sizeZ - z - 1) * sizeX;
                            
                            // browse each line of the input and store in the output accordingly
                            for (int j = 0; j < sizeY; j++)
                            {
                                int inOffset = j * sizeX;
                                Object outSlice = Array.get(out_Z_XY, startOffset + j * sign);
                                
                                System.arraycopy(in_XY, inOffset, outSlice, outOffset, sizeX);
                            }
                        }
                    }
            
            break;
        }
        
        out.dataChanged();
        output.setValue(out);
    }
}
