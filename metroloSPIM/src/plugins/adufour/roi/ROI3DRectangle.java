package plugins.adufour.roi;

import icy.canvas.Canvas2D;
import icy.canvas.IcyCanvas;
import icy.roi.ROI3D;
import icy.sequence.Sequence;
import icy.type.point.Point3D;
import icy.type.rectangle.Rectangle3D;

import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import plugins.kernel.roi.roi2d.ROI2DRectangle;

public class ROI3DRectangle extends ROI3D
{
    private final Rectangle3D    bounds;
    
    private final ROI2DRectangle rectangleROI = new ROI2DRectangle();
    
    public ROI3DRectangle()
    {
        bounds = new Rectangle3D.Double(0, 0, 0, 0, 0, 0);
    }
    
    public ROI3DRectangle(Point3D topLeftUp, Point3D bottomRightDown)
    {
        this(new Rectangle3D.Double(topLeftUp.getX(), topLeftUp.getY(), topLeftUp.getZ(), bottomRightDown.getX(), bottomRightDown.getY(), bottomRightDown.getZ()));
    }
    
    public ROI3DRectangle(Rectangle3D bounds)
    {
        this.bounds = bounds;
        roiChanged();
    }
    
    /**
     * @return a copy of the bounds of this ROI
     */
    @Override
    public Rectangle3D computeBounds3D()
    {
        return new Rectangle3D.Double(bounds.getX(), bounds.getY(), bounds.getZ(), bounds.getSizeX(), bounds.getSizeY(), bounds.getSizeZ());
    }
    
    @Override
    protected ROIPainter createPainter()
    {
        return new ROI3DRectanglePainter();
    }
    
    @Override
    public boolean contains(double x, double y, double z)
    {
        return bounds.contains(x, y, z);
    }
    
    @Override
    public boolean contains(double x, double y, double z, double sizeX, double sizeY, double sizeZ)
    {
        return bounds.contains(x, y, z, sizeX, sizeY, sizeZ);
    }

    @Override
    public boolean hasSelectedPoint()
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean intersects(double x, double y, double z, double sizeX, double sizeY, double sizeZ)
    {
        return bounds.intersects(x, y, z, sizeX, sizeY, sizeZ);
    }
    
    @Override
    public double getPerimeter()
    {
        return bounds.getSizeX() * bounds.getSizeY() * 2 + bounds.getSizeX() * bounds.getSizeZ() * 2 + bounds.getSizeY() * bounds.getSizeZ() * 2;
    }
    
    @Override
    public double getVolume()
    {
        return bounds.getSizeX() * bounds.getSizeY() * bounds.getSizeZ();
    }
    
    @Override
    public void roiChanged()
    {
        super.roiChanged();
        rectangleROI.setBounds2D(new Rectangle2D.Double(bounds.getX(), bounds.getY(), bounds.getSizeX(), bounds.getSizeY()));
        rectangleROI.setZ((int) bounds.getZ());
    }
    
    public class ROI3DRectanglePainter extends ROIPainter
    {
        @Override
        public void paint(Graphics2D g, Sequence sequence, IcyCanvas canvas)
        {
            super.paint(g, sequence, canvas);
            
            if (canvas instanceof Canvas2D)
            {
                ROIPainter overlay = rectangleROI.getOverlay();
//                if (!overlay.isAttached(sequence)) overlay.
                
                int z = canvas.getPositionZ();
                
                if (z >= bounds.getZ() && z < bounds.getZ() + bounds.getSizeZ())
                {
                    rectangleROI.setZ(z);
                    overlay.paint(g, sequence, canvas);
                }
            }
            else if (canvas.getClass().getSimpleName().equalsIgnoreCase("OrthoCanvas"))
            {
                
            }
            else
            {
//                rectangleROI.getPainter().detachFrom(sequence);
                System.err.println("Unsupported canvas: " + canvas.getClass().getSimpleName());
            }
        }
    }
}
